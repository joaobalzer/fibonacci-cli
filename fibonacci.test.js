import fibonacci from "./index.js";

describe('Fibonacci', () => {
    test('should return 1 when input equals 1', () => {
      const inputNumber = parseInt(1)
      const result = fibonacci(inputNumber);
      expect(result).toEqual(1);
    });
    
    test('should return 1 when input equals 2', () => {
        const inputNumber = parseInt(2)
        const result = fibonacci(inputNumber);
        expect(result).toEqual(1);
      });
    
    test('should return 2 when input equals 3', () => {
        const inputNumber = parseInt(3)
        const result = fibonacci(inputNumber);
        expect(result).toEqual(2);
      });

    test('should return 3 when input equals 4', () => {
        const inputNumber = parseInt(4)
        const result = fibonacci(inputNumber);
        expect(result).toEqual(3);
      });

    test('should return 5 when input equals 5', () => {
        const inputNumber = parseInt(5)
        const result = fibonacci(inputNumber);
        expect(result).toEqual(5);
      });
});
